#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "Shapes.h"

#define F_RED     "\x1b[31m"
#define F_GREEN   "\x1b[32m"
#define F_YELLOW  "\x1b[33m"
#define F_BLUE    "\x1b[34m"
#define F_MAGENTA "\x1b[35m"
#define F_CYAN    "\x1b[36m"
#define F_RESET   "\x1b[0m"

int main(int argc, char *argv[])
{
	 
	printf(F_RED 		"\tMethod1 \tArea: %0.3f \n" 	F_RESET,	Method1(42, CIRCLE));
	printf(F_GREEN 		"\tMethod2 \tArea: %0.3f \n" 	F_RESET, 	Method2(42, CIRCLE));
	printf(F_YELLOW 	"\tMethod3 \tArea: %0.3f \n" 	F_RESET, 	Method3(42, CIRCLE));
	printf(F_BLUE 		"\tMethod4 \tArea: %0.3f \n" 	F_RESET,	Method4(42, CIRCLE));
	printf(F_MAGENTA 	"\tMethod5 \tArea: %0.3f \n" 	F_RESET, 	Method5(42, CIRCLE));
	printf(F_CYAN 		"\tMethod6 \tArea: %0.3f \n" 	F_RESET,	Method6(42, CIRCLE));
	printf(F_RED 		"\tMethod7 \tArea: %0.3f \n" 	F_RESET,	Method7(42, CIRCLE));
	printf(F_GREEN 		"\tMethod8 \tArea: %0.3f \n" 	F_RESET, 	Method8(42, CIRCLE));
	printf(F_YELLOW 	"\tMethod9 \tArea: %0.3f \n"	F_RESET, 	Method9(42, CIRCLE));
	printf(F_BLUE 		"\tMethod10 \tArea: %0.3f \n" 	F_RESET,	Method10(42, CIRCLE));
	return 0;
}

